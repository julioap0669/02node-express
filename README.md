Primeros pasos:
    -para instalar ejecutar npm i en la raiz del proyecto
    -para ejecutar el servidor ejecutar en la consola: npm start

1) crear la ruta para obtener todos los productos
2) crear la ruta para obtener un producto por Id
3) crear la ruta para obtener un prodcuto por Id y nombre de proveedor
4) crear la ruta para Agregar este  Producto: 
                    {
                        "producto": {
                            "id": 3,
                            "type": "Bebida",
                            "marca": "Cunnington",
                            "proveedor": {
                                "id": 2,
                                "name": "Dulcenter",
                                "precio": 90
                            }
                        }    
                    }
5) crear la ruta para obtener todos los productos que pertenezcan a Dulcenter
            pista recordar que funcion de objetos devuelve uno y cual varios
6) crear la ruta para obtener todos los productos que valen mas de 100 pesos
            ayuda al final de la ruta debería ser: /precio?gt>100
            el signo ? indica que vamos a empezar agregar paramatros de tipo query
            al objeto req de la ruta que estamos ejecutando
7) crear la ruta para modificar un producto
8) crear la ruta para borrar un producto 

Extra:
9) cambiar el precio de manaos a 89
10) borrar un producto