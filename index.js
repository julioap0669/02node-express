// index.js  Principal

const express = require('express');
const app = express()
 
const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({extended:true})); // Ya podemos recibir datos post
app.use(bodyParser.json());
app.use(bodyParser.raw());

// Requiere, todas las rutas declaradas en archivo (Enrutador) index.js de la carpeta routes 
const rutas = require('./routes');
app.use('/', rutas);


// middlewares



// --------------------------- Declaracion del Puerto ----------------------------------------
app.listen(3000, () => {
    console.log('Servidor de Api Usuarios, Productos y Proveedores, inicializado y receptando peticiones por el puerto 3000');
});

