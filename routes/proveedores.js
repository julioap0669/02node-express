// Requiere, el express y su instancia, obteniendo el objeto router 
const router = require('express').Router();

// tProveedores es un array que representa o simula la tabla Proveedores de una Base de Datos
const tProveedores = require('../proveedoresBd');


//---------------------- Funciones de Respuesta, para las Rutas de Proveedores ----------------------

// 1 - Retorna todos los Proveedores, sin filtros
const getProveedores = () => {
    // Retorna la tabla (tProveedores) completa
    return tProveedores;
}


// 2 - Retorna un Proveedor, filtrado por id de Proveedores
const getProveedoresByIdProve = (pIdProve) => {
    return tProveedores.filter( (rProve) => rProve.id == pIdProve );
}


// 3 - Agrega un registro de Proveedor
const postProveedores = (pProve) => {
    // Agrega en la tabla (tProveedores), el registro (pProve), recibido como parametro en esta funcion 
    tProveedores.push(pProve);

    // Muestra en consola del Terminal, el registro agregado, recibido como parametro en esta funcion 
    console.log(pProve);
    
    // Retorna la tabla tProveedores resultante, de agregar un registro
    return tProveedores;
}


// 4 - Modifica un registro de Proveedor, por id de Proveedores 
const updateProveedoresByIdProve = (pIdProve, pProve) => {
    // Obtiene la posicion dentro del array, del Proveedor a modificar, por Id de Proveedores   
    let vPosProve = tProveedores.findIndex( (rProve) => rProve.id == pIdProve );
  
    // El metodo Splice() reemplaza el elemento del array en su posicion original
    tProveedores.splice(vPosProve, 1, pProve); 

    // Retorna la tabla tProveedores resultante, de modificar un registro
    return tProveedores
}


// 5 - Elimina un Proveedor, por id de Proveedores 
const deleteProveedoresByIdProve = (pIdProve) => {
    // Obtiene la posicion dentro del array, del Proveedor a eliminar por Id de Proveedores   
    let vPosProve = tProveedores.findIndex( (rProve) => rProve.id == pIdProve );
 
    // El metodo Splice() elimina el elemento del array
    tProveedores.splice(vPosProve, 1);   

    // Retorna la tabla tProveedores resultante, de eliminar un registro 
    return tProveedores;
}


//-------------------------------------- Rutas de Proveedores ----------------------------------- 

/* 1 - Ruta de Solicitud de Todos los proveedores
       URL: localhost:3000/proveedores            */
router.get('/', function (req, res) {
    res.json( getProveedores() );
})


/* 2 - Ruta de Solicitud de un Proveedor, filtrado por id de Proveedor
       URL: localhost:3000/proveedores/ y el literal del id o codigo de proveedor  */
router.get('/:pIdProve', function (req, res) {
    res.json( getProveedoresByIdProve(req.params.pIdProve) );
})


/* 3 - Ruta para Agregar un registro de Proveedor
       URL: localhost:3000/proveedores            */
router.post('/', function (req, res) {
    // Devuelve la tabla resultante, es decir Todos los registros con el agregado 
    res.json( postProveedores(req.body) );
})


/* 4 - Ruta para Modificar un registro de Proveedor, por id de Proveedor
       URL: localhost:3000/proveedores/ y el literal del id de proveedor  */
router.put('/:pIdProve', function (req, res) {
    // Devuelve la tabla resultante, es decir Todos los registros o elementos con el correspondiente modificado 
    res.json( updateProveedoresByIdProve(req.params.pIdProve, req.body) );
})


/* 5 - Ruta para Eliminar un registro de Proveedor, por id de Proveedor
       URL: localhost:3000/proveedores/ y el literal del id de proveedor  */ 
router.delete('/:pIdProve', function (req, res) {
    // Devuelve la tabla resultante, es decir sin el registro o elemento eliminado 
    res.json( deleteProveedoresByIdProve(req.params.pIdProve) );
})


// Exporta la instancia router, para que sea requerida en el index.js Enrutador
module.exports = router;
