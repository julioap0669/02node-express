// Requiere, el express y su instancia, obteniendo el objeto router 
const router = require('express').Router();

// tProductos es un array que representa o simula la tabla Productos de una Base de Datos
const tProductos = require('../productosBd');


// --------------------------- Funciones - Productos -----------------------------------------  
// Funciones de Respuesta, para las Rutas de Peticiones de Productos desde el Cliente FrontEnd

// 1 - Retorna todos los Productos, sin filtros
const getProductos = () => {
    // Retorna la tabla (tProductos) completa
    return tProductos;
}


// 2 - Retorna un Producto, filtrado por id de Productos (el primero que cumple la condicion)
const getProductosByIdProdu = (pIdProdu) => {
    /* En la tabla (tProductos), (const declarada al principio, en el require), 
       find busca y devuelve el primer registro (rProdu) (elemento del array) 
       cuyo campo  (rProdu.id)  es igual al parametro  (pIdProdu)  recibido en esta funcion */
    return tProductos.find( (rProdu) => rProdu.id == pIdProdu );
}


// 3 - Retorna un Producto, filtrado por id de Productos y Nombre de Proveedor (el primero que cumple la condicion)
const getProductosByIdProduAndNameProve = (pIdProdu, pNameProve) => {
    /* En la tabla (tProductos), (const declarada al principio, en el require), 
       find busca y devuelve el primer registro  (rProdu)  con 
       campo  (rProdu.id)  igual al parametro  (pIdProdu)  recibido en esta funcion, y
       campo  (rProdu.proveedor.name)  igual al parametro  (pNameProve)  recibido en esta funcion */
    return tProductos.find( (rProdu) => ((rProdu.id == pIdProdu) && (rProdu.proveedor.name == pNameProve)) );
}


// 4.2 - Agrega un registro de Producto
const postProductos = (pProdu) => {
    // Agrega en la tabla (tProductos), el registro (pProdu), recibido como parametro en esta funcion 
    tProductos.push(pProdu);

    // Muestra el registro agregado, recibido como parametro en esta funcion 
    console.log(pProdu);
    
    return pProdu;
}


// 5 - Retorna Productos, filtrados por Nombre de Proveedor
const getProductosByNameProve = (pNameProve) => {
    /* En la tabla (tProductos), 
       filter filtra todos los registros (rProdu) con 
       campo  (rProdu.proveedor.name)  igual al parametro (pNameProve) recibido en esta funcion */
    return tProductos.filter( (rProdu) => (rProdu.proveedor.name == pNameProve) );
}


// 6 - Retorna Productos, filtrados por Precio de Proveedor
const getProductosByPrecioProve = (pPrecioProve) => {
    /* En la tabla (tProductos), 
       filter filtra todos los registros (rProdu)  con 
       campo  (rProdu.proveedor.precio)  igual al parametro (pPrecioProve) recibido en esta funcion */
    return tProductos.filter( (rProdu) => (rProdu.proveedor.precio > pPrecioProve) );
}


// 7 - Modifica un Producto, por id de Productos 
const updateProductosByIdProdu = (pIdProdu, pProdu) => {
    // Obtiene la posicion dentro del array, del producto a modificar, por Id de Producto   
    let vPosProdu = tProductos.findIndex( (rProdu) => rProdu.id == pIdProdu );
  
    // El metodo Splice(), elimina el elemento a modificar
    //tProductos.splice(vPosProdu, 1); 
    
    /* El metodo push, agrega al final del array, el registro o elemento modificado, 
       recibido como parametro en el req.body de la ruta, y pasado a esta funcion
       Esta metodologia con 2 acciones, altera la posicion del elemento modificado, en el array  */
    //tProductos.push(pProdu);   

    /* Otra forma de realizar la modificacion, reemplazando el elemento del array en su posicion original
       El metodo Splice(), elimina el elemento de la posicion indicada en el 1º Parametro vPosProdu, 
       e inserta el elemento del 3º Parametro pProdu, en la misma posicion */
    tProductos.splice(vPosProdu, 1, pProdu); 

    return tProductos
}


// 8 - Elimina un Producto, por id de Productos 
const deleteProductosByIdProdu = (pIdProdu) => {
    // Muestra en la consola del Terminal, el campo clave del registro a eliminar, recibido por parametro en la ruta y enviado a esta funcion
    console.log(pIdProdu);
   
    // Obtiene la posicion dentro del array, del producto a eliminar por Id de Producto   
    let vPosProdu = tProductos.findIndex( (rProdu) => rProdu.id == pIdProdu );
 
    // Muestra en la consola del Terminal, la poscicion del elemento a eliminar
    console.log(vPosProdu);

    // Obtiene el registro Producto (elemento del Array, que se encuentra en la posicion obtenida con findIndex())
    // Esto tambien podria obtenerse con find(), como se realizo en las consultas get
    //let rProduEli = tProductos[vPosProdu];       
  
    /* El metodo Splice() puede: Agregar  elementos al array o Eliminar elementos del array o ambas acciones juntas
       El primer parametro indica la posicion desde en la que se realizaran las acciones agregar o eliminar
       El segundo parametro indica la cantidad de elementos a eliminar desde la posicion indicada en parametro1 (0 no elimina elementos)
       El o los parametros siguientes, son los elementos a agregar o insertar, en la posicion indicada en parametro1
       El metodo de array.splice(), devuelve los elementos eliminados. El array resultante queda en la misma variable de origen */ 
    let rProduEli = tProductos.splice(vPosProdu, 1);   

    // Esta es otra forma de obtener la tabla, sin el registro que se desea eliminar
    // En lugar de eliminar un registro, se obtienen los otros, filtrando los que tienen id distinto del que se debe eliminar 
    // tProductos.filter( (rProdu) => (rProdu.id != pIdProdu) );   

    return tProductos;
}


//---------------------------- Rutas - Productos --------------------------------------------- 
// Rutas de Peticiones de Productos, desde el Cliente FrontEnd

/* 1 - Ruta de Solicitud de Todos los Productos
   En navedagor de internet poner direccion:
      localhost:3000/productos */
router.get('/', function (req, res) {
    res.json(getProductos());
})


/* 2 - Ruta de Solicitud de un Producto, filtrado por id de Producto
   En navedagor de internet poner direccion:
      localhost:3000/productos/ y el literal del id o codigo de producto */
router.get('/:pIdProdu', function (req, res) {
    /* Si en el nombre de la Ruta /productos/:pIdProdu  
       se especifica : , lo que sigue es un parametro del objeto request o req, 
       por lo tanto con  req.params.pIdProdu se obtiene el parametro pIdProdu de la ruta
       Devuelve el registro obtenido */
    res.json(getProductosByIdProdu(req.params.pIdProdu));
})


/* 3 - Ruta de Solicitud de un Producto, filtrado por id de Producto y name de Proveedor
   En navedagor de internet poner direccion: 
      localhost:3000/productos/ y el literal del id del Producto  /proveedor/ y el literal del nombre del Proveedor */
router.get('/:pIdProdu/proveedor/:pNameProve', function(req,res) {
    let rProdu = getProductosByIdProduAndNameProve(req.params.pIdProdu, req.params.pNameProve);
    if (rProdu) {
        // Devuelve el registro obtenido 
        res.json(rProdu);
    } else {
        res.send("No existen productos, con los datos solicitados.");
    }
})


/* 4.1 - Ruta para agregar un registro de Producto, resuelto en la funcion de la ruta
   Se prueba desde aplicacion Postman o un Html con formulario,
   No desde el navegador de internet, porque este solo realiza consultas mediante el metodo get 
   En Postman poner direccion: 
      localhost:3000/productos   */
router.post('/', function (req, res) {
    /* Body es un parametro del objeto request o req, que contiene el registro enviado desde el FrontEnd
       Si el objeto registro contenido en body, posee nombre, por Ej: producto 
       se puede acceder al mismo con req.Body o req.body.producto */
    let rProdu = (req.body.producto); 
    
    // Agrega en la tabla tProductos, el registro rProdu
    tProductos.push(rProdu);    
    
    // Muestra en la consola del Terminal, el registro agregado
    console.log(rProdu);
           
    // Devuelve el registro agregado 
    res.json(rProdu);
})


/* 4.2 - Ruta para Agregar un registro de Producto, mediante una funcion ejecutada desde la funcion de la ruta
   Se prueba desde aplicacion Postman o un Html con formulario,
   No desde el navegador de internet, porque este solo realiza consultas mediante el metodo get 
   En Postman poner direccion: 
      localhost:3000/productos/funcion   */
router.post('/funcion', function (req, res) {
    /* Body es un parametro del objeto request o req, que contiene el registro enviado desde el FrontEnd
       Si el objeto registro contenido en body, no posee nombre, 
       se accede al mismo con req.Body */
    let rProdu = postProductos(req.body);                   
       
    // Devuelve el registro agregado 
    res.json(rProdu);
})


/* 5 - Ruta de Solicitud de Productos, filtrados por name de Proveedor
   En navedagor de internet poner direccion: 
      localhost:3000/productos/ y el literal del nombre del Proveedor   */
router.get('/proveedor/:pNameProve', function(req,res) {
    let rProdu = getProductosByNameProve(req.params.pNameProve);
    if (rProdu) {
        // Devuelve los registros obtenidos 
        res.json(rProdu);
    } else {
        res.send("No existen productos, con el Nombre de Proveedor solicitado.");
    }
})


/* 6 - Ruta de Solicitud de Productos, filtrados por precio de Proveedor mayor a, con parametro query
   Los query params son todos aquellos que van al final de la URL seguido del símbolo de interrogación (?). 
   Se pueden enviar cuantos Query params se requiera, siempre y cuando no excedamos el límite máximo de caracteres para un URL. 
   Cada parámetro deberá está separado con el símbolo de ampersand (&). 
   Ej:   /cources?coupon=FREE&Source=Google

   En navedagor de internet poner direccion: 
      localhost:3000/productos/proveedor/precio/precio?pPrecioProve=100   
   El objeto request o req poseera el parametro pPrecioProve y su valor sera 100 */
router.get('/proveedor/precio/precio', function(req,res) {
    let rProdu = getProductosByPrecioProve(req.query.pPrecioProve);
    if (rProdu) {
        // Devuelve los registros obtenidos 
        res.json(rProdu);
    } else {
        res.send("No existen productos, con precio mayor al solicitado.");
    }
})


/* 7 - Ruta para Modificar un registro de Producto, por id de Producto
   Se prueba desde aplicacion Postman o un Html con formulario,
   No desde el navegador de internet, porque este solo realiza consultas mediante el metodo get 
   En Postman poner direccion: 
      localhost:3000/productos      */
router.put('/:pIdProdu', function (req, res) {
    /* Modifica en la tabla tProductos, el registro o elemento del array,
       cuyo Id de Producto es igual al indicado en el parametro  pIdProdu  de la ruta,
       con el registro o elemento de array, recibido como parametro en el body del objeto req (req.body) 
       La Funcion de modificacion, retorna la tabla resultante */
    let tProduRes = updateProductosByIdProdu(req.params.pIdProdu, req.body);
             
    // Devuelve la tabla resultante, es decir Todos los registros o elementos con el correspondiente modificado 
    res.json(tProduRes);
})


/* 8 - Ruta para Eliminar un registro de Producto, por id de Producto
   Se prueba desde aplicacion Postman o un Html con formulario,
   No desde el navegador de internet, porque este solo realiza consultas mediante el metodo get 
   En Postman poner direccion: 
      localhost:3000/productos      */
router.delete('/:pIdProdu', function (req, res) {
    /* Elimina en la tabla tProductos, el registro o elemento del array,
       cuyo Id de Producto es igual al indicado en el parametro  pIdProdu  de la ruta 
       La Funcion de eliminacion, retorna la tabla resultante */
    let tProduRes = deleteProductosByIdProdu(req.params.pIdProdu);
             
    // Devuelve la tabla resultante, es decir sin el registro o elemento eliminado 
    // res.send('Se elimino el producto: ' + req.params.pIdProdu);
    res.json(tProduRes);
})


// Exporta la instancia router, para que sea requerida en el index.js Enrutador
module.exports = router;
