// index.js  Enrutador

// Requiere, el express y su instancia, obteniendo el objeto router 
const router = require('express').Router();

// Las siguientes dos declaraciones, pueden realizarse en una linea como la anterior
// const express = require('express');    // Requiere express     
// const router = express.Router();       // Crea instancia de express, obteniendo el objeto router


// Requiere las Rutas
const rutasUsuarios = require('./usuarios');
const rutasProductos = require('./productos');
const rutasProveedores = require('./proveedores');

// Pone en uso las Rutas Requeridas
router.use('/usuarios', rutasUsuarios);
router.use('/productos', rutasProductos);
router.use('/proveedores', rutasProveedores);


//---------------------------- Ruta - Pagina Principal --------------------------------------- 
// En navedagor de internet poner direccion localhost:3000
router.get('/', function (req, res) {
    //res.send('******* Pagina Principal de Api Usuarios, Productos y Proveedores obtenidos de array (Base de Datos) *******');
    res.status(200).send('******* Pagina Principal de Api Usuarios, Productos y Proveedores obtenidos de array (simulando Base de Datos) *******');
})


// Exporta la instancia router, para que sea requerida en el index.js Principal
module.exports = router;


