// Requiere, el express y su instancia, obteniendo el objeto router 
const router = require('express').Router();

// tUsuarios es un array que representa o simula la tabla Usuarios de una Base de Datos
// Al ser un array tiene todos los metodos de Arrays
const tUsuarios = require('../usuariosBd');


//---------------------------- Funciones - Usuarios ----------------------------------------- 
// Funciones de Respuesta, para las Rutas de Peticiones de Usuarios desde el Cliente FrontEnd

// Declaracion de funciones tipo arrow
const getUsuarioById = (userId) => {
    return tUsuarios.find( (user) => user.id == userId );
}


const getUsuarios = () => {
    return tUsuarios;
}


// Aca declaramos funciones del la manera tradicional
function getUsuarioByName ( name ) {
    return tUsuarios.find( (user) => user.name == name );
}


function findByIdAndPosition(id, position) {
    return tUsuarios.find( (user ) => user.id == id && user.job.position == position );
}


//---------------------------- Rutas - Usuarios ---------------------------------------------- 
// Rutas de Peticiones de Usuarios, desde el Cliente FrontEnd

router.get('/', function (req, res) {
    res.json(getUsuarios());
})


router.get('/:id', function (req, res) {
    // Esto es para ver por la consola la variable que le pasamos a la funcion de Filtrado
    console.log(req.params.name);
    
    // Como nombre /users/:id  entonces en req.params tengo el parametro id
    let user = getUsuarioById(req.params.id);
    res.json(user);
})


/*
// Esta ruta es una prueba que hicimos para busca por otro parametro que fue name
// Esta comentada porque no puede existir dos rutas iguales para el mismo metodo http 
// EJEMPLO:   no puede existir /users/:unaVariable y /users/:otraVariable

app.get('/:name', function (req, res) {
    // Esto es para ver por la consola la variable que le pasamos a la funcion console.log()
    console.log(req.params.name);
    // Como nombre /:name  entonces en req.params tengo el parametro name
    let user = getUsuarioByName(req.params.name);
    res.json(user);
})
*/
router.post('/', function (req, res) {
    let user = req.body.user;
    console.log(user);
    tUsuarios.push(user);
    res.json(user);
})


// Aca estoy definiendo una ruta nueva para pedir un recurso 
// Esta ruta tiene dos variables  id  y position. Ambas vienen en params
router.get('/:id/job/:position', function(req,res) {
    let user = findByIdAndPosition(req.params.id, req.params.position);
    if(user) {
        res.json(user)
    } else {
        res.send("no existe tal descripcion")
    }
})


// Exporta la instancia router, para que sea requerida en el index.js Enrutador
module.exports = router;