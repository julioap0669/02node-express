module.exports = [
    {
        id: 1,
        type: "Bebida",
        marca: "Coca Cola",
        proveedor: {
            id: 1,
            name: "Mayorista Vital",
            precio: 100
        }
    },
    {
        id: 2,
        type: "Bebida",
        marca: "Manaos",
        proveedor: {
            id: 2,
            name: "Dulcenter",
            precio: 110
        }
    },
    {
        id: 3,
        type: "Bebida",
        marca: "Cunnington",
        proveedor: {
            id: 2,
            name: "Dulcenter",
            precio: 90
        }
    }
]