module.exports = [
    {
        id: 1,
        nombre: "Mayorista Vital",
        direccion: "San Martin 1024",
        idCiudad: "R3",
        idProvincia: "03"
    },
    {
        id: 2,
        nombre: "Dulcenter",
        direccion: "Colon 895",
        idCiudad: "Cor",
        idProvincia: "03"
    },
    {
        id: 3,
        nombre: "Hiper Rosario",
        direccion: "Belgrano 1220",
        idCiudad: "Ros",
        idProvincia: "12"
    }
]