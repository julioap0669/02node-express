module.exports = [
    {
        id:1,
        name:"Bruno",
        surname:"Mendez",
        job: {
            id: 1,
            position: "Fullstack"
        }
    },
    {
        id:2,
        name:"Leonardo",
        surname:"Mendez",
        job: {
            id: 2,
            position: "Developer"
        }
    }
]